import { AbstractControlOptions, AsyncValidatorFn, ValidatorFn, FormControl } from "@angular/forms";
import { Subscription } from "rxjs";
import { debounceTime } from "rxjs";

export interface IExtendedAbstractControl {
    lable?: string;
    name: string;
    icon?: string;
    placeholder?: string;
    validation?: { [key: string]: any };
    errorMessages?: Array<string>;
}

export class FormControlModel extends FormControl implements IExtendedAbstractControl {
    lable?: string;
    name: string;
    icon?: string;
    placeholder?: string;
    validation?: { [key: string]: any };
    errorMessages?: Array<string>;

    private sub: Subscription;
    private debounce = 500;

    constructor(
        config: IExtendedAbstractControl,
        formState: any = null,
        validatorOrOpts?: ValidatorFn | AbstractControlOptions | ValidatorFn[],
        asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[]
    ) {
        super(formState, validatorOrOpts, asyncValidator);

        this.lable = config.lable;
        this.name = config.name;
        this.icon = config.icon;
        this.placeholder = config.placeholder;
        this.validation = config.validation;

        this.sub = this.valueChanges.pipe(
            debounceTime(this.debounce)
        ).subscribe(() => {
            this.errorMessages = [];
            if (this.errors && this.dirty) {
                Object.keys(this.errors).forEach(messagekey => {
                    if (this.validation?.[messagekey]) {
                        this.errorMessages?.push(this.validation[messagekey]);
                    }
                });
            }
        });
    }

    public unsubscribe(): void {
        this.sub.unsubscribe();
    }

}