import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormControlComponent } from './form-control.component';
import { FormControlModel } from 'src/app/models/form.model';
import { By } from '@angular/platform-browser';

describe('FormControlComponent', () => {
  let component: FormControlComponent;
  let fixture: ComponentFixture<FormControlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormControlComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormControlComponent);
    component = fixture.componentInstance;
    component.control = new FormControlModel({
      lable: 'Email',
      placeholder: 'Email',
      name: 'Email',
      validation: {
        required: 'Please enter your email.',
        pattern: 'Wrong email pattern.',
      },
      icon: 'fas fa-envelope',
    }, '', [])
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.control.lable).toEqual("Email");
  });
});
