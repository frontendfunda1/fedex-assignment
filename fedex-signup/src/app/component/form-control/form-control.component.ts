import { Component, Input, OnDestroy } from '@angular/core';
import { FormControlModel } from 'src/app/models/form.model';

@Component({
  selector: 'app-form-control',
  templateUrl: './form-control.component.html',
  styleUrls: ['./form-control.component.scss']
})
export class FormControlComponent implements OnDestroy {

  @Input() control: FormControlModel;

  constructor() { }

  ngOnDestroy(): void {
    this.control.unsubscribe();
  }
}
