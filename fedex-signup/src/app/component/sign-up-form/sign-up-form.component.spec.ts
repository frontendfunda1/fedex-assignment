import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignUpFormComponent } from './sign-up-form.component';
import { SignUpService } from '../../services/sign-up.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';



describe('SignUpFormComponent', () => {
  let component: SignUpFormComponent;
  let fixture: ComponentFixture<SignUpFormComponent>;
  let service: SignUpService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [SignUpFormComponent],
      providers: [SignUpService],
    })
      .compileComponents();
    service = TestBed.inject(SignUpService);
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create user', () => {

    let request = {
      "firstName": "Vasanth",
      "lastName": "Sekar",
      "email": "vasanth@gmail.com"
    }

    service.postUser(request).subscribe((res) => {

      expect(component.userName).toEqual("Vasanth Sekar");
      expect(component.isLoading).toEqual(false);
      expect(component.isUserCreated).toEqual(true);
    })
  });
});
