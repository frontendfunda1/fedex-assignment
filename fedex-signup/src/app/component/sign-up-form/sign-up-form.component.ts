import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { FormControlModel } from 'src/app/models/form.model';
import { SignUpService } from '../../services/sign-up.service';

@Component({
  selector: 'app-sign-up-form',
  templateUrl: './sign-up-form.component.html',
  styleUrls: ['./sign-up-form.component.scss']
})
export class SignUpFormComponent implements OnInit, OnDestroy {

  public form: FormGroup;
  postUser$: Subscription;
  isUserCreated: boolean;
  userName: string;
  isLoading: boolean;
  errorScreen: boolean;

  constructor(private signUpService: SignUpService) { }

  ngOnInit(): void {
    this.isUserCreated = false;
    this.isLoading = false;
    this.errorScreen = false;
    this.form = new FormGroup({
      firstName: new FormControlModel({
        lable: 'First Name',
        placeholder: 'First Name',
        name: 'first name',
        validation: {
          required: 'Please enter your first name.',
          minlength: 'Your first name must be at least 3 characters.',
          pattern: 'Please enter only characters',
        },
        icon: 'fas fa-user',
      }, '', [
        Validators.required,
        Validators.minLength(3),
        Validators.pattern("^[a-zA-Z_ ]+$")
      ]),
      lastName: new FormControlModel({
        lable: 'Last Name',
        placeholder: 'Last Name',
        name: 'last name',
        validation: {
          required: 'Please enter your last name.',
          minlength: 'Your last name must be at least 3 characters.',
          pattern: 'Please enter only characters',
        },
        icon: 'fas fa-user',
      }, '', [
        Validators.required,
        Validators.minLength(3),
        Validators.pattern("^[a-zA-Z_ ]+$")
      ]),
      email: new FormControlModel({
        lable: 'Email',
        placeholder: 'Email',
        name: 'Email',
        validation: {
          required: 'Please enter your email.',
          pattern: 'Wrong email pattern.',
        },
        icon: 'fas fa-envelope',
      }, '', [
        Validators.required,
        Validators.pattern("^([a-z0-9\d\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?$")
      ])
    });

  }

  public onSubmit(): void {
    this.isLoading = true;
    this.postUser$ = this.signUpService.postUser(this.form.value).subscribe((response) => {

      if (response.firstName && response.lastName) {
        this.userName = response.firstName + ' ' + response.lastName;
        this.isUserCreated = true;
      } else {
        this.errorScreen = true;
      }
      this.isLoading = false;
    }, () => {
      this.errorScreen = true;
    });
  }

  public getControls(): Array<FormControlModel> {
    return Object.values(this.form.controls) as Array<FormControlModel>;
  }

  ngOnDestroy(): void {
    if (this.postUser$) {
      this.postUser$.unsubscribe();
    }
  }

}
