import { TestBed } from '@angular/core/testing';

import { SignUpService } from './sign-up.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('SignUpService', () => {
  let service: SignUpService;
  let httpController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SignUpService]
    }).compileComponents();

    service = TestBed.inject(SignUpService);
    httpController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should check the return response', () => {
    let request = {
      "firstName": "Vasanth",
      "lastName": "Sekar",
      "email": "vasanth@gmail.com"
    }

    service.postUser(request).subscribe((res) => {

      expect(res.firstName).toEqual("Vasanth");

      const url = 'https://demo-api.vercel.app/users';

      const req = httpController.expectOne(url);

      expect(req.request.method).toEqual('POST');
    })
  });


});
