import { Injectable } from '@angular/core';
import { USER_POST_URL } from './sign-up.constant';
import { User } from './sign-up.interface';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SignUpService {

  constructor(private readonly httpService: HttpClient) { }

  postUser(
    userRequest: User
  ): Observable<any> {
    return this.httpService.post(USER_POST_URL, userRequest).pipe(
      catchError(this.handleError)
    );;
  }

  handleError(error: HttpErrorResponse) {
    console.log("Something went wrong!");
    return throwError(() => error);
  }

}
