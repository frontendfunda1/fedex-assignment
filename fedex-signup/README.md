# FedexSignup Assignment

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.0.3.

# Project SetUp

Clone the repository and do `npm install` , thats it the assignment is ready.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Implemented CI/CD Pipeline

`npm install, npm install -g @angular/cli, ng lint, npm run test:ci, ng build & Deploy to AWS S3 Bucket` to part of pipeline in Bitbucket. The build artifacts will be stored in the `dist/` directory.

## AWS S3 Bucket URL

Output can be viewed here : `http://vasanthsekar.s3-website.eu-west-3.amazonaws.com/`

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Stack Used

Angular 13 , Reactive Form, HTML/SCSS, TypeScript , Javascript, Karma, Jasmine, Bitbucket, CI/CD Pipeline, AWS S3 Bucket, AWS Static hosting Enabled.

## Responsive Screen

It will adapt to mobile screens 
